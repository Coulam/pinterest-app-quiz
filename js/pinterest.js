import {
    getPinterestSettings as settings
} from './settings';
let appSettings = settings();

export class Pinterest {
    constructor() {
        PDK.init({
            appId: appSettings.PIN_APP,
            cookie: true
        });
    }

    login(callback) {
        PDK.login({
            scope: appSettings.PIN_SCOPE
        }, callback);
    }

    getUserInfo(callback) {
        PDK.request('/me/', 'GET', callback);
    }

    logout() {
        PDK.logout();
    }

    loggedIn() {
        return !!PDK.getSession();
    }

    createPin(data, callback) {
        PDK.request('/v1/pins/', 'POST', data, callback);
    }

    createBoard(data, callback) {
        PDK.request('/boards/', 'POST', data, callback);
    }

    getBoardData(data, callback) {
        PDK.request('/boards/'+data+"/pins/", 'GET', callback);
    }

    myBoards(callback) {
        PDK.me('boards', {
            fields: appSettings.PIN_FIELDS
        }, callback);
    }
}