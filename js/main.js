// TODO - returns only up to 25 pins per request before pagination required

//imports
import {
	Pinterest
} from './pinterest';
import {
	getBoardSettings as boardSettings
} from './settings';

//vars
let appSettings = boardSettings();
let pinterest = new Pinterest();
let pins = [];
var imageData = [
    {
        boardName: "cats",
        boardData: [
            {
                note: "Cute Cat Pic",
                link: "https://uk.pinterest.com/pin/Aea5FV55b9pfNYWNAnPZwkBj1bf0RNK1vJr_h5k2EY0mH_0e_l3B8IY/",
                image_url: "https://s-media-cache-ak0.pinimg.com/564x/fc/71/d6/fc71d65909b8637877432760e14c89fd.jpg"
            }, {
                note: "Cute Cat Pic",
                link: "https://uk.pinterest.com/pin/347832771211251295/",
                image_url: "https://s-media-cache-ak0.pinimg.com/564x/52/53/a5/5253a5eb800099a1dbf3925daa43fb89.jpg"
            }, {
                note: "Cute Cat Pic",
                link: "https://uk.pinterest.com/pin/347832771211251288/",
                image_url: "https://s-media-cache-ak0.pinimg.com/564x/d5/c2/89/d5c2898a7e18fa266b3385973c354c2a.jpg"
            }
        ]
    }, {
        boardName: "dogs",
        boardData: [
            {
                note: "Cute Dog Pic",
                link: "https://uk.pinterest.com/pin/347832771211251261/",
                image_url: "https://s-media-cache-ak0.pinimg.com/564x/e6/5a/13/e65a13da4297ab59d66c2cb99cc9a6c0.jpg"
            }, {
                note: "Cute Dog Pic",
                link: "https://uk.pinterest.com/pin/347832771211251267/",
                image_url: "https://s-media-cache-ak0.pinimg.com/564x/1b/9b/f0/1b9bf09f28ef618c17e6bb1b29714867.jpg"
            }, {
                note: "Cute Dog Pic",
                link: "https://uk.pinterest.com/pin/347832771211251268/",
                image_url: "https://s-media-cache-ak0.pinimg.com/564x/66/d0/7e/66d07e7112dbf5f5ea81612fad2d8175.jpg"
            }
        ]
    }, {
        boardName: "fish",
        answers: [
            {
                note: "Fish Pic",
                link: "https://uk.pinterest.com/pin/347832771211251185/",
                image_url: "https://s-media-cache-ak0.pinimg.com/564x/8e/27/d9/8e27d9955ee40c87c9cee3204c4d8d61.jpg"
            }, {
                note: "Fish Pic",
                link: "https://uk.pinterest.com/pin/347832771211251179/",
                image_url: "https://s-media-cache-ak0.pinimg.com/564x/aa/c8/97/aac897078e2f67e83c64f52d688d771a.jpg"
            }, {
                note: "Fish Pic",
                link: "https://uk.pinterest.com/pin/347832771211251190/",
                image_url: "https://s-media-cache-ak0.pinimg.com/564x/50/2d/8a/502d8ae302860d2fd4f34aa447b1aa2e.jpg"
            }
        ]
    }, {
        boardName: "hamsters",
        answers: [
            {
                note: "Cute Hamster Pic",
                link: "https://uk.pinterest.com/pin/AS8d5MOhMdJExFAwNWgs86F96NktsWc1cyXUvQk2KIGvGWOeKxfGP9E/",
                image_url: "https://s-media-cache-ak0.pinimg.com/564x/78/b4/ee/78b4eee94b8be5c461fbb3cebc03285b.jpg"
            }, {
                note: "Cute Hamster Pic",
                link: "https://uk.pinterest.com/pin/347832771211251312/",
                image_url: "https://s-media-cache-ak0.pinimg.com/564x/00/9f/df/009fdf7a5dbc48e58ceaa9f3b1d40231.jpg"
            }, {
                note: "Cute Hamster Pic",
                link: "http://shiiranni.deviantart.com/art/Baby-hamster-344478000",
                image_url: "http://orig00.deviantart.net/8a7c/f/2012/359/e/f/baby_hamster_by_shiiranni-d5p3cxc.png"
            }
        ]
    }
];

export default imageData;


//binding
let loginBtn = document.getElementById("loginBtn");
let loading = document.getElementById("loadingText");

loginBtn.addEventListener("click", function () {
	login();
})

// Logs the user into pinterest
function login() {

	console.log("logging into Pinterest");

	pinterest.login(function () {
		showElement(loginBtn, false);
			
		getBoards(function () {
			createBoard("test-board", "Example of a board using the pinterest API", pins);
		});
	});
}

// Gets all the existing board data for the quiz
function getBoards(callback) {

	console.log("loading all boards");

	showElement(loading, true);
	loading.innerHTML = "loading pinterest board data";

	let promises = [];

	for (let obj of appSettings.PIN_BOARDS) {
		let promise = new Promise(function (resolve, reject) {
			pinterest.getBoardData(obj.board, function (response) {

				// Get the array of pins
				var returnArray = response.data;

				// Then figure out how many pins to return, based on the user's score
				returnArray.length = Math.round(obj.score / appSettings.PIN_TOTAL * appSettings.PIN_AMOUNT);

				// Now we can concactinate these pins to our final 'pin array' (our user-generated board)
				pins = pins.concat(returnArray);

				resolve();
			});
		});
		promises.push(promise);
	}

	Promise.all(promises).then(function () {
			console.log("all pinterest boards loaded");

			// When we get all of the pins, shuffle them so they are in a random order
			var shufflePins = pins;
			shuffle(shufflePins)

			callback();
		})
		.catch(function (error) {
			console.log(error);
		});
}

function createBoard(name, description, pinArray) {

	loading.innerHTML = "creating a pinterest board";

	pinterest.createBoard({
		name: "test-board",
		desc: description
	}, function (data) {
		console.log(data);
	});

	pinterest.getUserInfo(function (data) {
		console.log(data);
	});
	var user = "";


	for (let obj of imageData[1].boardData) {

		var o = {
			board: "joecoulam/test-board",
			note: obj.note,
			image_url: obj.image_url,
			link: obj.link
		}

		pinterest.createPin(o, function (data) {
			console.log("added", data);
		});
	}

	window.open("https://uk.pinterest.com/joecoulam/test-board/");
}

// Simple function to show or hide elements
function showElement(element, bool) {
	if (!bool) {
		element.style.visibility = "hidden";
	} else {
		element.style.visibility = "visible";
	}
}

// Shuffles an array
function shuffle(a) {
	for (let i = a.length; i; i--) {
		let j = Math.floor(Math.random() * i);
		[a[i - 1], a[j]] = [a[j], a[i - 1]];
	}
}