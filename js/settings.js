//  Settings containing app consts

const PIN_APP = "4903893339379610724"; // App ID
const PIN_FIELDS = "id,name,image[small]";
const PIN_SCOPE = "read_public, write_public"; // User permission access
const PIN_BOARDS = [{
    board: "joecoulam/cats",
    score: 2
}, {
    board: "joecoulam/dogs",
    score: 5
}, {
    board: "joecoulam/hamsters",
    score: 2
}, {
    board: "joecoulam/fish",
    score: 1
}]; // Score data taken from quiz
const PIN_AMOUNT = 25; // Number of pins in the total generated board


export function getPinterestSettings() {
    return {
        PIN_APP,
        PIN_FIELDS,
        PIN_SCOPE
    }
}

export function getBoardSettings() {

    let PIN_TOTAL = 0;

    for (let obj of PIN_BOARDS) {
        PIN_TOTAL += obj.score;
    }

    return {
        PIN_BOARDS,
        PIN_AMOUNT,
        PIN_TOTAL
    }
}