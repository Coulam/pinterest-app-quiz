"use strict";

System.register([], function (_export, _context) {
    "use strict";

    var PIN_APP, PIN_FIELDS, PIN_SCOPE, PIN_BOARDS, PIN_AMOUNT;
    // Number of pins in the total generated board


    function getPinterestSettings() {
        return {
            PIN_APP: PIN_APP,
            PIN_FIELDS: PIN_FIELDS,
            PIN_SCOPE: PIN_SCOPE
        };
    }

    _export("getPinterestSettings", getPinterestSettings);

    function getBoardSettings() {

        var PIN_TOTAL = 0;

        var _iteratorNormalCompletion = true;
        var _didIteratorError = false;
        var _iteratorError = undefined;

        try {
            for (var _iterator = PIN_BOARDS[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                var obj = _step.value;

                PIN_TOTAL += obj.score;
            }
        } catch (err) {
            _didIteratorError = true;
            _iteratorError = err;
        } finally {
            try {
                if (!_iteratorNormalCompletion && _iterator.return) {
                    _iterator.return();
                }
            } finally {
                if (_didIteratorError) {
                    throw _iteratorError;
                }
            }
        }

        return {
            PIN_BOARDS: PIN_BOARDS,
            PIN_AMOUNT: PIN_AMOUNT,
            PIN_TOTAL: PIN_TOTAL
        };
    }

    _export("getBoardSettings", getBoardSettings);

    return {
        setters: [],
        execute: function () {
            PIN_APP = "4903893339379610724";
            PIN_FIELDS = "id,name,image[small]";
            PIN_SCOPE = "read_public, write_public";
            PIN_BOARDS = [{
                board: "joecoulam/cats",
                score: 2
            }, {
                board: "joecoulam/dogs",
                score: 5
            }, {
                board: "joecoulam/hamsters",
                score: 2
            }, {
                board: "joecoulam/fish",
                score: 1
            }];
            PIN_AMOUNT = 25;
        }
    };
});
//# sourceMappingURL=settings.js.map
