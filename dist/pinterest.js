'use strict';

System.register(['./settings'], function (_export, _context) {
    "use strict";

    var settings, _createClass, appSettings, Pinterest;

    function _classCallCheck(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
            throw new TypeError("Cannot call a class as a function");
        }
    }

    return {
        setters: [function (_settings) {
            settings = _settings.getPinterestSettings;
        }],
        execute: function () {
            _createClass = function () {
                function defineProperties(target, props) {
                    for (var i = 0; i < props.length; i++) {
                        var descriptor = props[i];
                        descriptor.enumerable = descriptor.enumerable || false;
                        descriptor.configurable = true;
                        if ("value" in descriptor) descriptor.writable = true;
                        Object.defineProperty(target, descriptor.key, descriptor);
                    }
                }

                return function (Constructor, protoProps, staticProps) {
                    if (protoProps) defineProperties(Constructor.prototype, protoProps);
                    if (staticProps) defineProperties(Constructor, staticProps);
                    return Constructor;
                };
            }();

            appSettings = settings();

            _export('Pinterest', Pinterest = function () {
                function Pinterest() {
                    _classCallCheck(this, Pinterest);

                    PDK.init({
                        appId: appSettings.PIN_APP,
                        cookie: true
                    });
                }

                _createClass(Pinterest, [{
                    key: 'login',
                    value: function login(callback) {
                        PDK.login({
                            scope: appSettings.PIN_SCOPE
                        }, callback);
                    }
                }, {
                    key: 'getUserInfo',
                    value: function getUserInfo(callback) {
                        PDK.request('/me/', 'GET', callback);
                    }
                }, {
                    key: 'logout',
                    value: function logout() {
                        PDK.logout();
                    }
                }, {
                    key: 'loggedIn',
                    value: function loggedIn() {
                        return !!PDK.getSession();
                    }
                }, {
                    key: 'createPin',
                    value: function createPin(data, callback) {
                        PDK.request('/v1/pins/', 'POST', data, callback);
                    }
                }, {
                    key: 'createBoard',
                    value: function createBoard(data, callback) {
                        PDK.request('/boards/', 'POST', data, callback);
                    }
                }, {
                    key: 'getBoardData',
                    value: function getBoardData(data, callback) {
                        PDK.request('/boards/' + data + "/pins/", 'GET', callback);
                    }
                }, {
                    key: 'myBoards',
                    value: function myBoards(callback) {
                        PDK.me('boards', {
                            fields: appSettings.PIN_FIELDS
                        }, callback);
                    }
                }]);

                return Pinterest;
            }());

            _export('Pinterest', Pinterest);
        }
    };
});
//# sourceMappingURL=pinterest.js.map
