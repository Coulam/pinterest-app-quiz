'use strict';

System.register(['./pinterest'], function (_export, _context) {
  "use strict";

  var Pinterest;
  return {
    setters: [function (_pinterest) {
      Pinterest = _pinterest.Pinterest;
    }],
    execute: function () {
      console.log(Pinterest);
    }
  };
});
//# sourceMappingURL=app.js.map
