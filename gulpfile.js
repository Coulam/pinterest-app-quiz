var babel = require('gulp-babel'),
    sourcemaps = require('gulp-sourcemaps'),
    gulp = require('gulp'),
    es6Path = 'js/*.js',
    compilePath = 'dist',
    browserSync = require('browser-sync').create();

gulp.task('babel', function () {
    gulp.src([es6Path])
        .pipe(sourcemaps.init())
        .pipe(babel())
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(compilePath));
});

gulp.task('browser-sync', function () {
    browserSync.init({
        server: {
            baseDir: "./",
            proxy: "https://mysite.dev",
            https: {
                key: "./ssl/key.pem",
                cert: "./ssl/cert.pem"
            }
        }
    });
});

gulp.task('watch', function () {
    gulp.watch(es6Path, ['babel']);
});

gulp.task('default', ['browser-sync', 'babel', 'watch']);